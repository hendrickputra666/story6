from django.urls import path
from .views import *


urlpatterns = [
    path('', my_status, name='home'),
    path('profile/', profile, name='profile'),
]

