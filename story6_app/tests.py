from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import *
from django.http import HttpRequest
import unittest


class story6test (TestCase):

	def test_story6_url_is_exist(self):
	 	response = Client().get('')
	 	self.assertEqual(response.status_code,200)


	def test_story6_using_index_template(self):
		response = Client().get('')
		self.assertTemplateUsed(response, 'story6.html')


	def test_landing_page_is_completed(self):
		request = HttpRequest()
		response = my_status(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Hello, Apa kabar?', html_response)


	def test_model_can_create_new_todo(self):
		#Creating a new activity
		new_activity = Status.objects.create(status='Working on Story6')

		#Retreiving all available activity
		counting_all_available_todo = Status.objects.all().count()
		self.assertEqual(counting_all_available_todo,1)


	def test_form_validation_for_blank_items(self):
		form = EventForm(data = {'status': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors['status'],
			["This field is required."]
		)

	def test_story6_post_success_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post(
			'/', {'status':test})
		self.assertEqual(response_post.status_code, 302)

		response = Client().get('/')

		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)


	def test_story6_post_error_and_render_the_result(self):
		test = "Anonymous"
		response_post = Client().post(
			'/', {'status':''})
		self.assertEqual(response_post.status_code,200)

		response = Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertNotIn(test, html_response)


class profile (TestCase):

	def test_profile_url_is_exist(self):
	 	response = Client().get('/profile/')
	 	self.assertEqual(response.status_code,200)

	def test_profile_using_index_template(self):
		response = Client().get('/profile/')
		self.assertTemplateUsed(response, 'profile.html')

