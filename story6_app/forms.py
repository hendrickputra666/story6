from django import forms
from django.forms import ModelForm
from .models import Status

class EventForm(forms.ModelForm):
	class Meta: #menuju ke models
		model = Status
		fields = ['status']