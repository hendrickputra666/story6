from django.shortcuts import render
from django.shortcuts import reverse
from django.http import HttpResponse, HttpResponseRedirect
from .models import Status
from .forms import EventForm


def my_status(request):

	events = Status.objects.all().values()
	if request.method == "POST":
		form = EventForm(request.POST)
		if form.is_valid():
			new_event = form.save()
			return HttpResponseRedirect('/')
	else :
			form = EventForm()

	response = {'form':form, 'events':events}
	return render(request, "story6.html", response)


def profile (request):
	return render(request, "profile.html")

	